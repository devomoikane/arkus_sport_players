# Arkus Backend Challenge (Ruby)

### Ruby version
Ruby 3.1.1
Rails 7.0.4

### How to run the test suite
```console
bin/rails s
```

## Routes

### Download data from provided URI 
**POST** _/api/v1/players/reset-data_

### Get info from a player

**GET** _/api/v1/player/\<id\>_ *(Player with provided id)*

**GET** _/api/v1/player/random_ *(Random player)*

### Search for players

**GET** _/api/v1/players/search_

Params:
* **sport**: "basketball", "football", "baseball"
* **letter**: first letter of the last name
* **age**: single age
* **ages**: range of ages with format **X-X**
* **position**: position of the player

