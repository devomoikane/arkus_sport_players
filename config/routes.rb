Rails.application.routes.draw do
  scope '/api/v1' do
    post 'players/reset-data', to: 'players#reset_data'
    get 'player/random', to: 'players#random_player'
    get 'player/:id', to: 'players#show'
    get 'players/search', to: 'players#search'
  end
end
