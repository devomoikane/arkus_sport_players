class CreatePlayers < ActiveRecord::Migration[7.0]
  def change
    create_table :players do |t|
      t.string :pro_status
      t.string :firstname
      t.string :fullname
      t.string :position
      t.string :throws
      t.string :pro_team
      t.string :lastname
      t.string :elias_id
      t.string :bats
      t.string :photo
      t.integer :eligible_for_offense_and_defense
      t.integer :age
      t.string :icons_headline
      t.integer :jersey
      t.string :icons_injury
      t.string :icons_suspension
      t.integer :bye_week
      t.string :sport
    end
  end
end
