# frozen_string_literal: true

class PlayerRepository < ApplicationRepository
  AGES_RANGE_MATCHER = "(\\d+)\\s*-\\s*(\\d+)"

  class << self
    def random
      scope.order(Arel.sql('RANDOM()')).first
    end

    def by_position_and_sport(position, sport)
      scope.where(sport: sport)
           .where(position: position)
    end

    def average_age_by_position_and_sport(position, sport)
      scope.where(sport: sport)
           .where(position: position)
           .average(:age)
    end

    def search(params)
      current_scope = scope
      if params.key?("sport")
        puts("getting by sport")
        current_scope = current_scope.where(sport: params["sport"])
      end

      if params.key?("letter")
        current_scope = current_scope.where("lastname LIKE '#{params["letter"]}%'")
      end

      if params.key?("age")
        current_scope = current_scope.where(age: params["age"])
      elsif params.key?("ages") && params["ages"].match?(AGES_RANGE_MATCHER)
        ages = params["ages"].match(AGES_RANGE_MATCHER)
        current_scope = current_scope.where("age >= #{ages[1]} AND age <= #{ages[2]}")
      end

      if params.key?("position")
        current_scope = current_scope.where(position: params["position"])
      end

      current_scope
    end
  end
end
