class PlayersController < ApplicationController
  def reset_data
    PlayerImportJsonService.for()
    render json: { message: "Completed import of data"}, status: :created
  end

  def random_player
    render json: PlayerPresenter.new(PlayerRepository.random).json
  end

  def show
    render json: PlayerPresenter.new(PlayerRepository.find(params[:id])).json
  end

  def search
    render json: PlayerPresenter.json_collection(PlayerRepository.search(params))
  end
end
