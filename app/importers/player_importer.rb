class PlayerImporter < ApplicationImporter
  def attributes
    {
      id: params[:id],
      pro_status: params[:pro_status],
      firstname: params[:firstname],
      fullname: params[:fullname],
      position: params[:position],
      throws: params[:throws],
      pro_team: params[:pro_team],
      lastname: params[:lastname],
      elias_id: params[:elias_id],
      bats: params[:bats],
      photo: params[:photo],
      eligible_for_offense_and_defense: params[:eligible_for_offense_and_defense],
      age: params[:age],
      icons_headline: params[:icons_headline],
      jersey: params[:jersey],
      icons_injury: params[:icons_injury],
      icons_suspension: params[:icons_suspension]
    }
  end
end
