# frozen_string_literal: true

class ApplicationImporter
  def initialize(record, params)
    @record = record
    @params = params.with_indifferent_access
  end

  def import
    record.update!(attributes)
  end

  def attributes
    {}
  end

  private

  attr_reader :record, :params
end
