# frozen_string_literal: true

class PlayerImportJsonService < ApplicationService
  def initialize(params= { sports: %w[baseball basketball football] })
    @params = params
  end

  def process
    PlayerRepository.destroy_all
    @params[:sports].each do |sport|
      process_sport(sport)
    end
  end

  def process_sport(sport)
    uri = URI.parse("https://api.cbssports.com/fantasy/players/list?version=3.0&SPORT=#{sport}&response_format=JSON")

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)

    response = http.request(request)

    if response.code == "200"
      result = JSON.parse(response.body)

      result["body"]["players"].each do |player_data|
        if player_data.key?("icons")
          icons = player_data["icons"]
          player_data[:icons_headline] = icons["headline"]
          player_data[:icons_injury] = icons["injury"]
          player_data[:icons_suspension] = icons["suspension"]
          player_data.delete("icons")
        end
        player_data[:sport] = sport
        ActiveRecord::Base.transaction do
          PlayerImporter.new(Player.create(player_data), player_data).import
        rescue StandardError => e
          puts("Duplicated key #{e}")
        end
      end
    end
  end

  private

  attr_reader :params
end
