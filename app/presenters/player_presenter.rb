# frozen_string_literal: true

class PlayerPresenter < ApplicationPresenter
  ATTRS = %i[id firstname lastname position age]
  METHODS = %i[name_brief average_position_age_diff]

  def name_brief
    return "N/A" if attributes["firstname"].nil? || attributes["lastname"].nil?

    case attributes["sport"]
    when "baseball"
      "#{attributes["firstname"][0]}. #{attributes["lastname"][0]}."
    when "basketball"
      "#{attributes["firstname"]} #{attributes["lastname"][0]}."
    when "football"
      "#{attributes["firstname"][0]}. #{attributes["lastname"]}"
    else
      "N/A"
    end
  end

  def average_position_age_diff
    return "N/A" if attributes["age"].nil?

    avg_age = PlayerRepository.average_age_by_position_and_sport(attributes["position"], attributes["sport"])
    attributes["age"] - avg_age
  end
end
